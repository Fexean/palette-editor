# Palette Editor
This is a basic program for working with various color palette files.


### Features:
*  Import/Export palettes of various formats
*  Change colors and their order
*  Undo/Redo
*  Index images/Replace image palette


### Supported formats:
*  JASC Palettes (.pal)
*  GIMP Palettes (.gpl)
*  RIFF Palettes (.pal)
*  Paint.net Palettes (.txt)
*  Adobe Color Table (.act)
*  PNG Images (.png)
*  Indexed PNG Color tables (.png)
*  Binary RGB555 (.gbapal)
*  Binary RGBA32
*  Binary RGBA888


### Building:
Open .pro file in Qt Creator and compile. Alternatively you can run `qmake` in the project directory to generate a Makefile.