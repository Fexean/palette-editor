
#include "PaletteModel.h"
#include <QMessageBox>
#include "UndoCommands.h"






PaletteModel::PaletteModel(QObject *parent, QUndoStack* undoStack) : QAbstractTableModel(parent), undostack(undoStack){
	dragmode = PalDragMode::SWAP;
	blendRate = 0.5;
    corepal.colors.resize(256, Color::rgb888(127, 127, 127));
}

int PaletteModel::rowCount(const QModelIndex& parent) const{ //height
	auto isPrime = [](int n){
        for(int i = 2;i<=std::sqrt(n);i++){
			if(n % i == 0) return false;
		}
		return true;
	};

	if(!getColorCount()){return 0;}

	int height = std::sqrt(getColorCount());
	if(isPrime(getColorCount())){
		height++;
	}else{
		while(getColorCount() % height){
			height--;
		}
	}

	return height;
}

int PaletteModel::columnCount(const QModelIndex & parent) const{ //width
	if(!getColorCount()){return 0;}


	unsigned int height = rowCount(parent);
	unsigned int width = getColorCount()/height;

	if(width * height != getColorCount()){
		width++;
	}
	return width;
}




QVariant PaletteModel::data(const QModelIndex & index, int role) const{
	if(!indexWithingPal(index) || role == Qt::CheckStateRole){
		return QVariant::Invalid;
	}
	const int idx = (index.column()+index.row()*columnCount(QModelIndex()));
	return corepal.at(idx).asQColor();
}


Qt::ItemFlags PaletteModel::flags(const QModelIndex& index) const{
	if(!index.isValid()){
		return Qt::ItemIsEnabled | Qt::ItemIsDropEnabled;
	}

	return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | Qt::ItemIsEditable;
}


bool PaletteModel::setData(const QModelIndex &index, const QVariant &value, int role){
	if(!indexWithingPal(index)){
		return false;
	}
	if(role == Qt::EditRole){
		undostack->push(new ColorChangeCommand(this, index, value.value<QColor>()));
		return true;
	}
	return false;
}



Qt::DropActions PaletteModel::supportedDropActions() const{
	return Qt::CopyAction | Qt::MoveAction;
}


QStringList PaletteModel::mimeTypes() const{
	QStringList types;
	types << "application/x-color";
	return types;
}

QMimeData* PaletteModel::mimeData(const QModelIndexList & indexes) const{
	QMimeData *mimeData = new QMimeData();
	QByteArray encodedData;
	QDataStream stream(&encodedData, QIODevice::WriteOnly);

	const QModelIndex index = indexes.at(0);
	if (indexWithingPal(index)) {
		QColor c = data(index, Qt::DisplayRole).value<QColor>();
		stream << c;
		stream << QColor(index.row(), index.column(), 0); //Hacky way to get drag source index to dropMimeData
		mimeData->setData("application/x-color", encodedData);
	}
	return mimeData;
}

bool PaletteModel::dropMimeData(const QMimeData * dat, Qt::DropAction action, int row, int column, const QModelIndex & parent){
	if (action == Qt::IgnoreAction)
		   return true;

	if (!dat->hasFormat("application/x-color"))
		return false;

	if (row == -1){
		if (parent.isValid())
			row = parent.row();
		else
			row = rowCount(QModelIndex());
	}

	if (column == -1){
		if (parent.isValid())
			column = parent.column();
		else
			column = columnCount(QModelIndex());
	}

	QByteArray encodedData = dat->data("application/x-color");
	QDataStream stream(&encodedData, QIODevice::ReadOnly);
	QColor color, originalPos;

	stream >> color;
	stream >> originalPos;
	int srcRow = originalPos.red();
	int srcCol = originalPos.green();

	QModelIndex dstIdx = index(row, column, QModelIndex());
	QModelIndex srcIdx = index(srcRow, srcCol, QModelIndex());

	if(!indexWithingPal(dstIdx) || !indexWithingPal(srcIdx)){
		return false;
	}


	QUndoCommand *cmd;

	switch(dragmode){
		case PalDragMode::SWAP:
		cmd = new QUndoCommand;
		new ColorChangeCommand(this, dstIdx, color, cmd);
		new ColorChangeCommand(this, srcIdx, data(dstIdx).value<QColor>(), cmd);
		undostack->push(cmd);
		break;

		case PalDragMode::CLONE:
		setData(dstIdx, color, Qt::EditRole);
		break;

		case PalDragMode::INSERT:
		undostack->push(new ColorInsertCommand(this, srcIdx, dstIdx));
		break;

		case PalDragMode::BLEND:
		blendColors(dstIdx, color);
		break;
	}

	return true;
}


void PaletteModel::resizePal(int colorcount){
    undostack->push(new PaletteResizeCommand(this, 0, colorcount));
}

void PaletteModel::limitPal(int start, int count){
    undostack->push(new PaletteResizeCommand(this, start, count));
}

uint PaletteModel::getColorCount() const{
	return corepal.colors.size();
}

void PaletteModel::update(){
	emit dataChanged(index(0, 0), index(rowCount(QModelIndex()), columnCount(QModelIndex())));
}

bool PaletteModel::indexWithingPal(const QModelIndex& idx) const{
	if(!idx.isValid()){
		return false;
	}
	return (idx.column() + idx.row()*columnCount(QModelIndex())) < getColorCount();
}

void PaletteModel::blendColors(const QModelIndex& dstIdx, const QColor& color){
	QColor dst = data(dstIdx).value<QColor>();
	int r = blendRate*color.red() + (1.0-blendRate)*dst.red();
	int g = blendRate*color.green() + (1.0-blendRate)*dst.green();
	int b = blendRate*color.blue() + (1.0-blendRate)*dst.blue();
	setData(dstIdx, QColor(r, g, b), Qt::EditRole);
}



