#include "dialogs/PaletteFileDialog.h"
#include "core/palette.hpp"


PaletteFileDialog::PaletteFileDialog(QWidget* parent, const QString& file, int format, uint fileoffset) : QDialog(parent, Qt::Window | Qt::WindowFlags()){

    palformat = format;
    filepath = file;
    offset = fileoffset;

    formatLabel = new QLabel(tr("Palette Format"));
    fileLabel = new QLabel(tr("File"));
    formatBox = new QComboBox();
    filepathLine = new QLineEdit(file);
    offsetLine = new QLineEdit();

    QString offsetStr;
    offsetStr.setNum(fileoffset, 16);
    offsetLine->setText(offsetStr);

    formatLabel->setBuddy(formatBox);
    fileLabel->setBuddy(filepathLine);


    buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    browseButton = new QPushButton(tr("Browse..."));

    formatBox->addItems({
                            "JASC Palette (.pal)",
                            "GIMP Palette (.gpl)",
                            "RIFF Palette (.pal)",
                            "Adobe Color Table (.act)",
                            "RGB555 (Little endian)",
                            "RGB555 (Big endian)",
                            "RGB888 (Little endian)",
                            "RGBA8888 (Little endian)",
                            "PNG Palette",
                            "PNG Picture",
                            "Paint.Net Palette (.txt)",
			    "Nintendo Color Resource (.NCLR)"
                        });
    formatBox->setCurrentIndex(format);

    mainlayout = new QVBoxLayout;


    mainlayout->addWidget(fileLabel);
    mainlayout->addWidget(filepathLine);
    mainlayout->addWidget(browseButton);
    mainlayout->addSpacing(15);

    mainlayout->addWidget(formatLabel);
    mainlayout->addWidget(formatBox);
    mainlayout->addSpacing(15);

    mainlayout->addWidget(new QLabel(tr("File Offset (Hex)")));
    mainlayout->addWidget(offsetLine);
    mainlayout->addSpacing(30);

    mainlayout->addWidget(buttons);

    setLayout(mainlayout);

    connect(buttons, &QDialogButtonBox::accepted, this, [this](){
        palformat = formatBox->currentIndex();
        filepath = filepathLine->text();

	bool offsetOk;
        offset = offsetLine->text().toInt(&offsetOk, 16);

	if(offsetOk)
	        accept();
	else
		offsetLine->setStyleSheet("background-color:rgb(255, 120, 120);");
    });

    connect(buttons, &QDialogButtonBox::rejected, this, &QDialog::reject);
    connect(browseButton, &QPushButton::clicked, this, &PaletteFileDialog::browseFiles);

}






PaletteFileDialog* PaletteFileDialog::exportDialog(QWidget* parent, const QString& file, int format, uint fileoffset){
    PaletteFileDialog* dialog = new PaletteFileDialog(parent, file, format, fileoffset);
    dialog->buttons->button(QDialogButtonBox::Ok)->setText(tr("Export"));
    dialog->setWindowTitle(tr("Export Palette"));
    dialog->mode = Mode::EXPORT;
    return dialog;
}





PaletteFileDialog* PaletteFileDialog::importDialog(QWidget* parent, const QString& file, int format, uint fileoffset){
    PaletteFileDialog* dialog = new PaletteFileDialog(parent, file, format, fileoffset);
    dialog->buttons->button(QDialogButtonBox::Ok)->setText(tr("Import"));
    dialog->setWindowTitle(tr("Import Palette"));
    dialog->mode = Mode::IMPORT;
    return dialog;
}







void PaletteFileDialog::browseFiles(){
    QString file;
    currentDir = QDir(filepathLine->text()).absolutePath();

    if(mode == Mode::EXPORT){
        file = QFileDialog::getSaveFileName(this, tr("Import Palette"), currentDir, tr("All Files (*)"), nullptr, QFileDialog::DontConfirmOverwrite);
    }else{
        file = QFileDialog::getOpenFileName(this, tr("Import Palette"), currentDir, tr("All Files (*)"));
    }

    if(file.size()){
        filepathLine->setText(file);

        PalFormat format_ = Palette::getFileFormat(file);
        if(format_ != INVALID){
            formatBox->setCurrentIndex(format_);
        }
    }
}


