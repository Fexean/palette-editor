#include "dialogs/SettingsDialog.h"
#include <QVBoxLayout>
#include <QLabel>

SettingsDialog::SettingsDialog(QWidget* parent, LineFeedType defaultNewline, int defaultEditor) : QDialog(parent, Qt::Window |  Qt::WindowFlags())
{
    newlineBox = new QComboBox;
    newlineBox->addItems({
                             "CRLF",
                             "LF",
                             "CR",
                         });
    newlineBox->setCurrentIndex((int)defaultNewline);

    editorBox= new QComboBox;
    editorBox->addItems({
                             "H+SV Colordialog",
                             "QColorDialog",
                         });
    editorBox->setCurrentIndex(defaultEditor);

    buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    QVBoxLayout *mainlayout = new QVBoxLayout;

    mainlayout->addWidget(new QLabel(tr("Newline:")));
    mainlayout->addWidget(newlineBox);
    mainlayout->addSpacing(10);
    mainlayout->addWidget(new QLabel(tr("Colordialog:")));
    mainlayout->addWidget(editorBox);
    mainlayout->addSpacing(20);
    mainlayout->addWidget(buttons);

    setWindowTitle(tr("Settings"));
    setLayout(mainlayout);


    connect(buttons, &QDialogButtonBox::accepted, this, [this](){
        newline = (LineFeedType)newlineBox->currentIndex();
        editorType = editorBox->currentIndex();
        accept();
    });

    connect(buttons, &QDialogButtonBox::rejected, this, &QDialog::reject);
}
