#include "dialogs/pngindexingdialog.h"

#include <QVBoxLayout>
#include <QFileDialog>
#include <QPushButton>
#include <QLabel>
#include <QDialogButtonBox>
#include <QRadioButton>
#include <QImage>

PngIndexingDialog::PngIndexingDialog(QWidget* parent) : QDialog(parent, Qt::Window | Qt::WindowFlags())
{
    QVBoxLayout* mainlayout = new QVBoxLayout();
    filepathLine = new QLineEdit();
    QPushButton *browseButton = new QPushButton("Browse...");
    QDialogButtonBox* buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);

    radioReplacePal = new QRadioButton("Replace existing palette");
    radioIndex = new QRadioButton("Index image with palette");
    radioReplacePal->setChecked(true);

    mainlayout->addWidget(new QLabel(tr("PNG File:")));
    mainlayout->addWidget(filepathLine);
    mainlayout->addWidget(browseButton);
    mainlayout->addSpacing(15);

    mainlayout->addWidget(radioReplacePal);
    mainlayout->addWidget(radioIndex);
    mainlayout->addSpacing(30);

    mainlayout->addWidget(buttons);
    setLayout(mainlayout);
    setWindowTitle("Insert Palette To Image");

    connect(buttons, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(buttons, &QDialogButtonBox::rejected, this, &QDialog::reject);
    connect(browseButton, &QPushButton::clicked, this, &PngIndexingDialog::browseFiles);
}





void PngIndexingDialog::browseFiles(){
   QString file;
   QString currentDir = QDir(filepathLine->text()).absolutePath();

   file = QFileDialog::getOpenFileName(this, tr("Select Image"), currentDir, tr("PNG Images (*.png)"));
   if(file.size()){
       filepathLine->setText(file);
   }
}
