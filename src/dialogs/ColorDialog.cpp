#include "dialogs/ColorDialog.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QFormLayout>
#include <QDialogButtonBox>
#include <QScreen>
#include <QPushButton>
#include <QApplication>

int ColorHuePicker::y2hue(int y)
{
    int d = height() - 2*coff - 1;

    return 359 - (y - coff)*359/d;
}

int ColorHuePicker::hue2y(int h)
{
    int d = height() - 2*coff - 1;
    return coff + (359-h)*d/359;
}

ColorHuePicker::ColorHuePicker(QWidget* parent)
    :QWidget(parent)
{
    hue = 0;
    pix = nullptr;
}

ColorHuePicker::~ColorHuePicker()
{
    delete pix;
}

void ColorHuePicker::mouseMoveEvent(QMouseEvent *m)
{
    setHue(y2hue(m->pos().y()));
}
void ColorHuePicker::mousePressEvent(QMouseEvent *m)
{
    setHue(y2hue(m->pos().y()));
}



void ColorHuePicker::setHue(int h)
{
    if (hue == h){return;}
    hue = qMax(0, qMin(h,360));

    delete pix; pix=nullptr;
    repaint();
    emit newHue(hue);
}


void ColorHuePicker::paintEvent(QPaintEvent *)
{
    int w = width() - 5;

    QRect r(0, foff, w, height() - 2*foff);
    int wi = r.width() - 2;
    int hi = r.height() - 2;
    if (!pix || pix->height() != hi || pix->width() != wi) {
        delete pix;
        QImage img(wi, hi, QImage::Format_RGB32);
        int y;
        uint *pixel = (uint *) img.scanLine(0);
        for (y = 0; y < hi; y++) {
            uint *end = pixel + wi;
            std::fill(pixel, end, QColor::fromHsv(y2hue(y + coff), 255, 255).rgb());
            pixel = end;
        }
        pix = new QPixmap(QPixmap::fromImage(img));
    }
    QPainter p(this);
    p.drawPixmap(1, coff, *pix);
    const QPalette &g = palette();
    qDrawShadePanel(&p, r, g, true);
    p.setPen(g.windowText().color());
    p.setBrush(g.windowText());
    QPolygon a;
    int y = hue2y(hue);
    a.setPoints(3, w, y, w+5, y+5, w+5, y-5);
    p.eraseRect(w, 0, 5, height());
    p.drawPolygon(a);
}

void ColorHuePicker::setHue_(int h)
{
    hue = h;
    delete pix; pix=nullptr;
    repaint();
}



// ColorHuePicker end



QPoint QColorPicker::colPt()
{
    QRect r = contentsRect();
    return QPoint((val) * (r.width() - 1) / 255, (255 - sat) * (r.height() - 1) / 255);
}

int QColorPicker::huePt(const QPoint &pt)
{
    QRect r = contentsRect();
    return 360 - pt.x() * 360 / (r.width() - 1);
}

int QColorPicker::satPt(const QPoint &pt)
{
    QRect r = contentsRect();
    return 255 - pt.y() * 255 / (r.height() - 1);
}

int QColorPicker::valPt(const QPoint &pt){
    QRect r = contentsRect();
    return pt.x() * 255 / (r.width() - 1);
}

void QColorPicker::setCol(const QPoint &pt)
{
    setCol(valPt(pt), satPt(pt));
}

QColorPicker::QColorPicker(QWidget* parent)
    : QFrame(parent)
    , crossVisible(true)
{
    hue = 0; sat = 0;
    setCol(150, 255);

    setAttribute(Qt::WA_NoSystemBackground);
    setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed) );
}

QColorPicker::~QColorPicker()
{
}

void QColorPicker::setCrossVisible(bool visible)
{
    if (crossVisible != visible) {
        crossVisible = visible;
        update();
    }
}

QSize QColorPicker::sizeHint() const
{
    return QSize(200 + 2*frameWidth(), 200 + 2*frameWidth());
}

void QColorPicker::setCol(int v, int s)
{
    int nval = qMin(qMax(0,v), 255);
    int nsat = qMin(qMax(0,s), 255);
    if (nval == val && nsat == sat)
        return;

    QRect r(colPt(), QSize(20,21));
    val = nval; sat = nsat;
    r = r.united(QRect(colPt(), QSize(20,21)));
    r.translate(contentsRect().x()-9, contentsRect().y()-9);
    //    update(r);
    repaint(r);
}

void QColorPicker::setHue(int h){
    setHue_noEmit(h);
    emit newCol(hue, sat, val);
}

void QColorPicker::setHue_noEmit(int h){
    int nhue = qMin(qMax(0,h), 359);
    if (nhue == hue)
        return;
    hue = nhue;
    int w = width() - frameWidth() * 2;
    h = height() - frameWidth() * 2;
    QImage img(w, h, QImage::Format_RGB32);
    int x, y;
    uint *pixel = (uint *) img.scanLine(0);
    for (y = 0; y < h; y++) {
        const uint *end = pixel + w;
        x = 0;
        while (pixel < end) {
            QPoint p(x, y);
            QColor c;
            c.setHsv(hue, satPt(p), valPt(p));
            *pixel = c.rgb();
            ++pixel;
            ++x;
        }
    }
    pix = QPixmap::fromImage(img);
    repaint();
}

void QColorPicker::mouseMoveEvent(QMouseEvent *m)
{
    QPoint p = m->pos() - contentsRect().topLeft();
    setCol(p);
    emit newCol(hue, sat, val);
}

void QColorPicker::mousePressEvent(QMouseEvent *m)
{
    QPoint p = m->pos() - contentsRect().topLeft();
    setCol(p);
    emit newCol(hue, sat, val);
}

void QColorPicker::paintEvent(QPaintEvent* )
{
    QPainter p(this);
    drawFrame(&p);
    QRect r = contentsRect();

    p.drawPixmap(r.topLeft(), pix);

    if (crossVisible) {
        QPoint pt = colPt() + r.topLeft();
        p.setPen(Qt::black);
        p.setBrush(Qt::white);
        p.drawRect(pt.x()-9, pt.y(), 9, 2);
        p.drawRect(pt.x()+1, pt.y(), 9, 2);
        p.drawRect(pt.x(), pt.y()+2, 2, 9);
        p.drawRect(pt.x(), pt.y()-9, 2, 9);
    }
}

void QColorPicker::resizeEvent(QResizeEvent *ev)
{
    QFrame::resizeEvent(ev);

    int w = width() - frameWidth() * 2;
    int h = height() - frameWidth() * 2;
    QImage img(w, h, QImage::Format_RGB32);
    int x, y;
    uint *pixel = (uint *) img.scanLine(0);
    for (y = 0; y < h; y++) {
        const uint *end = pixel + w;
        x = 0;
        while (pixel < end) {
            QPoint p(x, y);
            QColor c;
            c.setHsv(hue, satPt(p), valPt(p));
            *pixel = c.rgb();
            ++pixel;
            ++x;
        }
    }
    pix = QPixmap::fromImage(img);
}




//QColorPicker end




ColorComponentSlider::ColorComponentSlider(const QString& label, uint maxValue, QWidget* parent) : QWidget(parent) {
    max = maxValue;

    slider = new QSlider(Qt::Horizontal, this);
    slider->setRange(0, max);

    box = new QSpinBox(this);
    box->setRange(0, max);

    text = new QLabel(label);

    internalLayout = new QHBoxLayout(this);

    internalLayout->addWidget(text);
    internalLayout->addWidget(box);
    internalLayout->addWidget(slider);
    setLayout(internalLayout);

    connect(slider, &QSlider::valueChanged, this, [this](int n){
        if(box->value() != n){
            box->setValue(n);
            emit valueChanged(n);
        }
    });

    connect(box, QOverload<int>::of(&QSpinBox::valueChanged), this, [this](int n){
        if(n != slider->value()){
            slider->setValue(n);
            emit valueChanged(n);
        }
    });
}

void ColorComponentSlider::setValue(int n){
    if(slider->value() != n && box->value() != n && n <= max){
        slider->blockSignals(true);
        box->blockSignals(true);
        slider->setValue(n);
        box->setValue(n);
        slider->blockSignals(false);
        box->blockSignals(false);
    }
}

int ColorComponentSlider::value() const{
    return slider->value();
}


QSize ColorComponentSlider::sizeHint() const{
    return QSize(200, 20);
}



ColorDisplay::ColorDisplay(QWidget* parent) : QWidget(parent){}

void ColorDisplay::setColor(const QColor& c){
    m_color = c;
}

QColor ColorDisplay::color(){
    return m_color;
}

void ColorDisplay::paintEvent(QPaintEvent *event){
    QPainter p(this);
    p.setPen(QColor::fromRgb(150,150,150));
    p.setBrush(m_color);
    p.drawRect(event->rect().x(), event->rect().y(), event->rect().width() - 1, event->rect().height() - 1);
}

void ColorDisplay::mouseReleaseEvent(QMouseEvent *event){
    const QPoint pos = event->pos();
    const int x = pos.x();
    const int y = pos.y();
    if(x > 0 && y> 0 && x < width() && y < height()){
        emit clicked();
    }
}

void ColorDisplay::setCol(int h, int s, int v){
    m_color.setHsv(h, s, v);
    repaint();
}



ColorDialog::ColorDialog(const QColor& initial, QWidget *parent) : QDialog(parent, (Qt::Dialog | Qt::WindowTitleHint | Qt::WindowSystemMenuHint | Qt::WindowCloseButtonHint)){
    init();
    setPrevColor(initial);
    newHsv(initial.hue(), initial.saturation(), initial.value());
}

ColorDialog::ColorDialog(QWidget *parent) : QDialog(parent, (Qt::Dialog | Qt::WindowTitleHint | Qt::WindowSystemMenuHint | Qt::WindowCloseButtonHint)){
    init();
}



void ColorDialog::init(){
    pickScreenColorEventFilter = 0;

    QGridLayout* mainlayout = new QGridLayout;
    mainlayout->setSizeConstraint(QLayout::SetFixedSize);

    QPushButton* pickScreenColorButton = new QPushButton(tr("Pick Screen Color"));

    QDialogButtonBox* buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(buttons, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(buttons, &QDialogButtonBox::rejected, this, &QDialog::reject);


    QHBoxLayout* pickerLayout = new QHBoxLayout;
    QFormLayout* colorLayout = new QFormLayout;
    colorSlider = new ColorHuePicker(this);
    colorMap = new QColorPicker(this);

    colorSlider->setFixedWidth(20);


    pickerLayout->addWidget(colorMap);
    pickerLayout->addWidget(colorSlider);
    mainlayout->addLayout(pickerLayout, 0, 0);


    prevColor = new ColorDisplay(this);
    currentColor = new ColorDisplay(this);
    currentColor->setColor(m_color);
    currentColor->setColor(m_color);
    colorLayout->addRow(tr("Current:"), currentColor);
    colorLayout->addRow(tr("Previous:"), prevColor);
    mainlayout->addLayout(colorLayout, 1, 0);

    mainlayout->addWidget(buttons, 2, 0);
    setLayout(mainlayout);
    setWindowTitle(tr("Select a color"));

    connect(colorSlider, &ColorHuePicker::newHue, colorMap, &QColorPicker::setHue);
    connect(colorMap, &QColorPicker::newCol, this, &ColorDialog::newHsv);


    QVBoxLayout *sliderLayout = new QVBoxLayout;
    hSlider = new ColorComponentSlider("H", 359, this);
    sSlider = new ColorComponentSlider("S", 255, this);
    vSlider = new ColorComponentSlider("V", 255, this);
    rSlider = new ColorComponentSlider("R", 255, this);
    gSlider = new ColorComponentSlider("G", 255, this);
    bSlider = new ColorComponentSlider("B", 255, this);
    sliderLayout->addWidget(hSlider);
    sliderLayout->addWidget(sSlider);
    sliderLayout->addWidget(vSlider);
    sliderLayout->addWidget(rSlider);
    sliderLayout->addWidget(gSlider);
    sliderLayout->addWidget(bSlider);
    sliderLayout->addWidget(pickScreenColorButton);
    sliderLayout->setSpacing(0);
    mainlayout->addLayout(sliderLayout, 0, 1);

    connect(hSlider, &ColorComponentSlider::valueChanged, this, &ColorDialog::updateColorFromSlidersHSV);
    connect(sSlider, &ColorComponentSlider::valueChanged, this, &ColorDialog::updateColorFromSlidersHSV);
    connect(vSlider, &ColorComponentSlider::valueChanged, this, &ColorDialog::updateColorFromSlidersHSV);

    connect(rSlider, &ColorComponentSlider::valueChanged, this, &ColorDialog::updateColorFromSlidersRGB);
    connect(gSlider, &ColorComponentSlider::valueChanged, this, &ColorDialog::updateColorFromSlidersRGB);
    connect(bSlider, &ColorComponentSlider::valueChanged, this, &ColorDialog::updateColorFromSlidersRGB);

    connect(prevColor, &ColorDisplay::clicked, this, [this](){
        const QColor c = prevColor->color();
        newHsv(c.hue(), c.saturation(), c.value());
    });

    connect(pickScreenColorButton, &QPushButton::clicked, this, &ColorDialog::pickScreenColor);

#ifdef Q_OS_WIN32
    dummyTransparentWindow.resize(1, 1);
    dummyTransparentWindow.setFlags(Qt::Tool | Qt::FramelessWindowHint);

    updateTimer = new QTimer(this);
    QObject::connect(updateTimer, &QTimer::timeout, this, &ColorDialog::updatePickScreenColorWindows);
#endif

}

ColorDialog::~ColorDialog(){}

void ColorDialog::newHsv(int h, int s, int v){
    m_color.setHsv(h, s, v);
    currentColor->setCol(h, s, v);
    colorMap->setHue_noEmit(h);
    colorMap->setCol(v, s);
    colorSlider->setHue_(h);

    QColor c = currentColor->color();
    rSlider->setValue(c.red());
    gSlider->setValue(c.green());
    bSlider->setValue(c.blue());

    hSlider->setValue(h);
    sSlider->setValue(s);
    vSlider->setValue(v);
}

QColor ColorDialog::color(){
    return m_color;
}
void ColorDialog::setColor(const QColor& color){
    newHsv(color.hue(), color.saturation(), color.value());
}

void ColorDialog::setPrevColor(const QColor& color){
    prevColor->setCol(color.hue(), color.saturation(), color.value());
}

void ColorDialog::updateColorFromSlidersHSV(){
    const int h = hSlider->value();
    const int s = sSlider->value();
    const int v = vSlider->value();
    newHsv(h,s,v);
}


void ColorDialog::updateColorFromSlidersRGB(){
    QColor color;
    color.setRgb(rSlider->value(), gSlider->value(), bSlider->value());
    const int h = color.hue();
    const int s = color.saturation();
    const int v = color.value();

    //newHsv without modifying rgb sliders
    hSlider->setValue(h);
    sSlider->setValue(s);
    vSlider->setValue(v);
    m_color.setHsv(h, s, v);
    currentColor->setCol(h, s, v);
    colorMap->setHue_noEmit(h);
    colorMap->setCol(v, s);
    colorSlider->setHue_(h);
}


// Event filter to be installed on the dialog while in color-picking mode.
class QColorPickingEventFilter : public QObject {
public:
    explicit QColorPickingEventFilter(ColorDialog *dp, QObject *parent) : QObject(parent), m_dp(dp) {}

    bool eventFilter(QObject *, QEvent *event) override
    {
        switch (event->type()) {
        case QEvent::MouseMove:
            return m_dp->handleColorPickingMouseMove(static_cast<QMouseEvent *>(event));
        case QEvent::MouseButtonRelease:
            return m_dp->handleColorPickingMouseButtonRelease(static_cast<QMouseEvent *>(event));
        case QEvent::KeyPress:
            return m_dp->handleColorPickingKeyPress(static_cast<QKeyEvent *>(event));
        default:
            break;
        }
        return false;
    }

private:
    ColorDialog *m_dp;
};


QColor ColorDialog::grabScreenColor(const QPoint &p)
{
    QScreen *screen = QGuiApplication::primaryScreen();;
    const QPixmap pixmap = screen->grabWindow(0, p.x(), p.y(), 1, 1);
    QImage i = pixmap.toImage();
    return i.pixel(0, 0);
}

bool ColorDialog::handleColorPickingMouseMove(QMouseEvent* e){
    QColor selected = grabScreenColor(e->globalPos());
    currentColor->setCol(selected.hue(), selected.saturation(), selected.value());
    return true;
}

bool ColorDialog::handleColorPickingMouseButtonRelease(QMouseEvent* e){
    QColor selected = grabScreenColor(e->globalPos());
    newHsv(selected.hue(), selected.saturation(), selected.value());
    stopPickScreenColor();
    e->accept();
    return true;
}

bool ColorDialog::handleColorPickingKeyPress(QKeyEvent* e){
    if (e->key() == Qt::Key_Return || e->key() == Qt::Key_Enter) {
        QColor selected = grabScreenColor(QCursor::pos());
        newHsv(selected.hue(), selected.saturation(), selected.value());
        stopPickScreenColor();
    }else if(e->matches(QKeySequence::Cancel)){
        currentColor->setCol(m_color.hue(), m_color.saturation(), m_color.value());
        stopPickScreenColor();
    }
    e->accept();
    return true;
}

void ColorDialog::pickScreenColor(){
    grabMouse(Qt::CrossCursor);
    grabKeyboard();
    setMouseTracking(true);
    if(!pickScreenColorEventFilter){
        pickScreenColorEventFilter = new QColorPickingEventFilter(this, this);
    }
    installEventFilter(pickScreenColorEventFilter);

#ifdef Q_OS_WIN32
    // On Windows mouse tracking doesn't work over other processes's windows
    updateTimer->start(30);

    // HACK: Because mouse grabbing doesn't work across processes, we have to have a dummy,
    // invisible window to catch the mouse click, otherwise we will click whatever we clicked
    // and loose focus.
    dummyTransparentWindow.show();
#endif

}

void ColorDialog::stopPickScreenColor(){
    removeEventFilter(pickScreenColorEventFilter);
    releaseKeyboard();
    releaseMouse();
    setMouseTracking(false);
#ifdef Q_OS_WIN32
    updateTimer->stop();
    dummyTransparentWindow.setVisible(false);
#endif
}

void ColorDialog::updatePickScreenColorWindows(){
    static QPoint lastGlobalPos;
    QPoint newGlobalPos = QCursor::pos();
    if (lastGlobalPos == newGlobalPos){return;}
    lastGlobalPos = newGlobalPos;
    if (!rect().contains(mapFromGlobal(newGlobalPos))) {
        QColor selected = grabScreenColor(newGlobalPos);
        currentColor->setCol(selected.hue(), selected.saturation(), selected.value());
#ifdef Q_OS_WIN32
        dummyTransparentWindow.setPosition(newGlobalPos);
#endif
    }
}
