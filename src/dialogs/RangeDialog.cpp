#include "dialogs/RangeDialog.h"

#include <QDialogButtonBox>
#include <QVBoxLayout>
#include <QLabel>

RangeDialog::RangeDialog(QWidget* parent, int colorcount) : QDialog(parent, Qt::Window |  Qt::WindowFlags())
{
	countLine = new QSpinBox();
	startLine = new QSpinBox();
	
    countLine->setRange(1, 256);
    startLine->setRange(0, colorcount - 1);
	
    QDialogButtonBox* buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    QVBoxLayout *mainlayout = new QVBoxLayout;

    mainlayout->addWidget(new QLabel(tr("Starting color:")));
    mainlayout->addWidget(startLine);
    mainlayout->addWidget(new QLabel(tr("Color count:")));
    mainlayout->addWidget(countLine);
    mainlayout->addSpacing(20);
    mainlayout->addWidget(buttons);

    setWindowTitle(tr("Limit palette to a range"));
    setLayout(mainlayout);


    connect(buttons, &QDialogButtonBox::accepted, this, [this](){
        start = startLine->value();
        count = countLine->value();
        accept();
    });

    connect(buttons, &QDialogButtonBox::rejected, this, &QDialog::reject);
}
