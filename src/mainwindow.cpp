#include <QDebug>
#include <QInputDialog>
#include <QMessageBox>
#include <QJsonObject>
#include <QJsonArray>
#include <fstream>


#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialogs/PaletteFileDialog.h"
#include "dialogs/SettingsDialog.h"
#include "dialogs/pngindexingdialog.h"
#include "UndoCommands.h"
#include "dialogs/RangeDialog.h"

void MainWindow::UpdateGridSize(){
    for(int i = 0;i<16;i++){
        ui->tableView->hideRow(i);
        ui->tableView->hideColumn(i);
    }

    for(int i = 0;i<PalModel->rowCount();i++){
        ui->tableView->showRow(i);

        for(int j = 0;j<PalModel->columnCount();j++){
            ui->tableView->showColumn(j);
        }
    }
}



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    ini(QCoreApplication::applicationDirPath() + "/settings.ini", QSettings::IniFormat, this)
{

    ui->setupUi(this);

    setAcceptDrops(true);

    undoStack = new QUndoStack;
    QAction* undoAction = undoStack->createUndoAction(this);
    undoAction->setShortcuts(QKeySequence::Undo);
    ui->menuPalette_Editor->addAction(undoAction);

    QAction* redoAction = undoStack->createRedoAction(this);
    redoAction->setShortcuts(QKeySequence::Redo);
    ui->menuPalette_Editor->addAction(redoAction);

    lastFormat = 0;
    lastFile = "";
    lastOffset = 0;
    newline = (LineFeedType)ini.value("newline", 0).value<int>();
    colordiag = (EditorType)ini.value("colordialog", 0).value<int>();

    for(int i = 0;i<MaxRecentFiles;i++){
        recentFileActs[i] = new QAction;
        ui->menuRecent_files->addAction(recentFileActs[i]);
        connect(recentFileActs[i], &QAction::triggered, this, &MainWindow::loadRecentFile);
    }
    updateRecentFiles();
    saveSettings();

    PalModel = new PaletteModel(ui->tableView, undoStack);

    ui->tableView->setModel(PalModel);

    palDelegate = new PaletteDelegate(ui->tableView);
    palDelegate->setEditor(colordiag);
    ui->tableView->setItemDelegate(palDelegate);

    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableView->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    UpdateGridSize();
    
    connect(ui->actionChange_Color_Count, &QAction::triggered, this, &MainWindow::openColorCountMenu);
    connect(ui->actionImport_Palette, &QAction::triggered, this, &MainWindow::openPaletteImporter);
    connect(ui->actionExport_Palette, &QAction::triggered, this, &MainWindow::openPaletteExporter);
    connect(ui->exportButton, &QPushButton::clicked, this, &MainWindow::openPaletteExporter);
    connect(ui->importButton, &QPushButton::clicked, this, &MainWindow::openPaletteImporter);
    connect(ui->actionClear_Palette, &QAction::triggered, this, &MainWindow::clearPalette);
    connect(ui->actionSave, &QAction::triggered, this, &MainWindow::onSave);
    connect(ui->actionSettings, &QAction::triggered, this, &MainWindow::openSettingsMenu);
    connect(ui->actionInsert_Palette_to_an_image, &QAction::triggered, this, &MainWindow::openImageIndexer);
    connect(ui->actionLimit_palette, &QAction::triggered, this, &MainWindow::openRangeDialog);

    connect(ui->radioButton, &QRadioButton::toggled, this, [this](){
        PalModel->dragmode = PalDragMode::SWAP;
    });

    connect(ui->radioButton_2, &QRadioButton::toggled, this, [this](){
        PalModel->dragmode = PalDragMode::INSERT;
    });

    connect(ui->radioButton_3, &QRadioButton::toggled, this, [this](){
        PalModel->dragmode = PalDragMode::CLONE;
    });
    
    connect(ui->radioButton_4, &QRadioButton::toggled, this, [this](){
        PalModel->dragmode = PalDragMode::BLEND;
    });

    connect(ui->blendRateBox, QOverload<int>::of(&QSpinBox::valueChanged), this, [this](int value){
        PalModel->blendRate = 0.01 * value;
    });

    connect(PalModel, &PaletteModel::paletteResized, this, &MainWindow::UpdateGridSize);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::dropEvent(QDropEvent* event){
    if(event->mimeData()->hasUrls()){
        QList<QUrl> urlList = event->mimeData()->urls();
        QString filepath = urlList.at(0).toLocalFile();
        importPalette(filepath);
    }
}



void MainWindow::importPalette(const QString& path){
    PalFormat format = Palette::getFileFormat(path);
    if(format != INVALID){
        if(!importPalette(path, format)){
            QMessageBox::warning(this, "Error", "Invalid palette file.", QMessageBox::Ok);
        }
    }else{
        PaletteFileDialog* dialog = PaletteFileDialog::importDialog(this, path, lastFormat);
        dialog->exec();
        if(dialog->result()){
            if(!importPalette(dialog->filepath, dialog->palformat, dialog->offset)){
                QMessageBox::warning(this, "Error", "Unable to import palette.", QMessageBox::Ok);
            }
        }
        delete dialog;
    }
}



void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasUrls())
        event->acceptProposedAction();
}


void MainWindow::openColorCountMenu(){
    bool ok;

    int colors = QInputDialog::getInt(this, tr("Set Palette Color Count"), tr("Colors:"), PalModel->getColorCount(), 1, 256, 1, &ok);
    if(ok){
        PalModel->resizePal(colors);
        //UpdateGridSize();
    }
}


void MainWindow::openRangeDialog(){
    RangeDialog dialog(this, PalModel->corepal.colors.size());
    dialog.exec();
    if(dialog.result()){
        PalModel->limitPal(dialog.start, dialog.count);
        //UpdateGridSize();
    }
}


void MainWindow::openPaletteImporter(){
    PaletteFileDialog* dialog = PaletteFileDialog::importDialog(this, lastFile, lastFormat, lastOffset);
    dialog->exec();

    if(dialog->result()){
        if(!importPalette(dialog->filepath, dialog->palformat, dialog->offset)){
            QMessageBox::warning(this, "Error", "Unable to open file.", QMessageBox::Ok);
        }
    }
    delete dialog;
}

void MainWindow::openPaletteExporter(){
    PaletteFileDialog* dialog = PaletteFileDialog::exportDialog(this, lastFile, lastFormat, lastOffset);
    dialog->exec();
    if(dialog->result()){
        if(!exportPalette(dialog->filepath, dialog->palformat, dialog->offset)){
            QMessageBox::warning(this, "Error", "Unable to save file.", QMessageBox::Ok);
        }
    }
    delete dialog;
}


void MainWindow::clearPalette(){
    undoStack->push(new PaletteClearCommand(PalModel));
}

void MainWindow::onSave(){
    if(lastFile.size()){
        if(!exportPalette(lastFile, lastFormat, lastOffset)){
            QMessageBox::warning(this, "Error", "Unable to save file.", QMessageBox::Ok);
        }
    }else{
        openPaletteExporter();
    }
}

bool MainWindow::importPalette(const QString& path, int format, uint offset){
    PaletteImporter importer(PalModel->corepal);
    if(importer.importPal(path, (PalFormat)format, offset)){
        lastFormat = format;
        lastFile = path;
        lastOffset = offset;
        UpdateGridSize();
        PalModel->update();
        addToRecentFiles(path, format, offset);
        return true;
    }
    return false;
}

bool MainWindow::exportPalette(const QString& path, int format, uint offset){
    PaletteExporter exporter(PalModel->corepal, newline);
    if(exporter.exportPal(path, (PalFormat)format, offset)){
        lastFormat = format;
        lastFile = path;
        lastOffset = offset;
        addToRecentFiles(path, format, offset);
        return true;
    }
    return false;
}


void MainWindow::openSettingsMenu(){
    SettingsDialog dialog(this, newline, (int)colordiag);
    dialog.exec();
    if(dialog.result()){
        newline = dialog.newline;
        colordiag = (EditorType)dialog.editorType;
        palDelegate->setEditor(colordiag);
        saveSettings();
    }
}

void MainWindow::saveSettings(){
    ini.setValue("newline", (int)newline);
    ini.setValue("colordialog", (int)colordiag);
}


void MainWindow::openImageIndexer(){
    PngIndexingDialog dialog(this);
    dialog.exec();
    if(dialog.result()){
        PaletteExporter exporter(PalModel->corepal, newline);
        if(!QFile(dialog.filepathLine->text()).exists()){
            QMessageBox::warning(this, "Error", "Selected file does not exist.", QMessageBox::Ok);
            return;
        }
        QImage img(dialog.filepathLine->text());
        if(img.format() == QImage::Format_Invalid){
            QMessageBox::warning(this, "Error", "Selected file is not a valid image.", QMessageBox::Ok);
            return;
        }
        if(dialog.radioReplacePal->isChecked()){
            if(img.format() != QImage::Format_Indexed8){
                QMessageBox::warning(this, "Error", "Selected image is not indexed.", QMessageBox::Ok);
                return;
            }
            exporter.exportImagePal(dialog.filepathLine->text());
        }else if(dialog.radioIndex->isChecked()){
            exporter.indexImageWithPal(dialog.filepathLine->text());
        }
    }
}


void MainWindow::addToRecentFiles(const QString &filepath, int format, int offset){
    QJsonObject obj;
    obj.insert("path", filepath);
    obj.insert("format", format);
    obj.insert("addr", offset);

    QJsonArray files = ini.value("recentFileList").toJsonArray();
    for(int i = 0;i<files.size();i++){
        if(files[i].toObject().value("path").toString() == filepath){
            files.removeAt(i);
        }
    }
    files.prepend(obj);
    ini.setValue("recentFileList", files);
    updateRecentFiles();
}

void MainWindow::loadRecentFile(){
    QAction *action = qobject_cast<QAction *>(sender());
    if(!action){
        return;
    }
    QJsonObject obj = action->data().toJsonObject();

    if (obj.contains("path") && obj.contains("format") && obj.contains("addr")){
        importPalette(obj.value("path").toString(), obj.value("format").toInt(), obj.value("addr").toInt());
    }
}

void MainWindow::updateRecentFiles(){
    QJsonArray files = ini.value("recentFileList").toJsonArray();
    int numRecentFiles = qMin(files.size(), (int)MaxRecentFiles);

    ui->menuRecent_files->setDisabled(!numRecentFiles);

    for (int i = 0; i < numRecentFiles; ++i) {
        QJsonObject x = files[i].toObject();
        QString text = QFileInfo(x.value("path").toString()).fileName();
        recentFileActs[i]->setText(text);
        recentFileActs[i]->setData(x);
        recentFileActs[i]->setVisible(true);
    }

    for (int j = numRecentFiles; j < MaxRecentFiles; ++j)
        recentFileActs[j]->setVisible(false);

}

