#include <QColor>
#include <QColorDialog>
#include "dialogs/ColorDialog.h"
#include "PaletteDelegate.h"

PaletteDelegate::PaletteDelegate(QObject *parent) : QStyledItemDelegate(parent){}


QWidget* PaletteDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem&, const QModelIndex& index) const{
    if(index.data() == QVariant::Invalid){
        return 0;
    }

    switch(editorType){
        case EditorType::QCOLORDIALOG:
            return new QColorDialog(parent);
        break;

        case EditorType::CUSTOM_COLORDIALOG:
            return new ColorDialog(index.data().value<QColor>(), parent);
        break;
    }
    return 0;
}


void PaletteDelegate::setEditorData(QWidget* editor, const QModelIndex& i) const{
    QColor c = i.model()->data(i, Qt::EditRole).value<QColor>();

    switch(editorType){
        case EditorType::QCOLORDIALOG:
            static_cast<QColorDialog*>(editor)->setCurrentColor(c);
        break;

        case EditorType::CUSTOM_COLORDIALOG:
            static_cast<ColorDialog*>(editor)->setColor(c);
        break;
    }
}

void PaletteDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const{

    switch(editorType){
        case EditorType::QCOLORDIALOG:
            if(static_cast<QColorDialog*>(editor)->result()){
                QColor value = static_cast<QColorDialog*>(editor)->currentColor();
                model->setData(index, value, Qt::EditRole);
            }
        break;

        case EditorType::CUSTOM_COLORDIALOG:
            if(static_cast<ColorDialog*>(editor)->result()){
                QColor value = static_cast<ColorDialog*>(editor)->color();
                model->setData(index, value, Qt::EditRole);
            }
        break;
    }

}

void PaletteDelegate::updateEditorGeometry(QWidget *editor,
    const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
    editor->setGeometry(option.rect);
}

void PaletteDelegate::setEditor(EditorType editor){
    editorType = editor;
}


void PaletteDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const{
    if(index.data() == QVariant::Invalid){
        return;
    }
    QColor color = index.data().value<QColor>();
    QColor border = (color.lightnessF() > 0.5) ? Qt::black : Qt::white;

    painter->save();
    if(option.state & QStyle::State_Selected){
        painter->setBrush(border);
        painter->setPen(border);
        painter->drawRect(option.rect);
        painter->setBrush(color);
        painter->setPen(color);
        painter->drawRect(option.rect.x() + 2, option.rect.y() + 2, option.rect.width() - 5, option.rect.height() - 5);
    }else{
        painter->setBrush(color);
        painter->setPen(color);
        painter->drawRect(option.rect);
    }
    painter->restore();
}
