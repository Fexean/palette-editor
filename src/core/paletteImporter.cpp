﻿#include "core/paletteImporter.hpp"
#include <QImage>


PaletteImporter::PaletteImporter(Palette &pal) : palette(pal) {}


void PaletteImporter::importBinaryRGB555(std::istream &in, int maxcolors){
    palette.colors.clear();
    union{
		char asBytes[2];
		uint16_t color16;
	};
	
    while(!in.eof() && !in.fail() && palette.colors.size() < maxcolors){
		in.read(asBytes, 2);
		if(in.eof() || in.fail()){
			return;
		}
		Color c = Color::rgb555(color16);
		palette.colors.push_back(c);
	}
}

void PaletteImporter::importBinaryRGB555_BigEndian(std::istream &in){
    palette.colors.clear();
    union{
		char asBytes[2];
		uint16_t color16;
	};
	
    while(!in.eof() && !in.fail() && palette.colors.size() < 256){
		in.read(asBytes, 2);
		if(in.eof() || in.fail()){
			return;
		}
		int tmp = asBytes[0];
		asBytes[0] = asBytes[1];
		asBytes[1] = tmp;
		Color c = Color::rgb555(color16);
		palette.colors.push_back(c);
	}
}


void PaletteImporter::importBinaryRGB888(std::istream &in){
    palette.colors.clear();
    union{
		char asBytes[4];
		uint32_t color32;
	};
	
	color32 = 0;
	
    while(!in.eof() && !in.fail() && palette.colors.size() < 256){
		in.read(asBytes, 3);
		if(in.eof() || in.fail()){
			return;
		}
		Color c = Color::rgb888(color32);
		palette.colors.push_back(c);
	}
}



void PaletteImporter::importRGBA32(std::istream& in){
    palette.colors.clear();
    union{
		char asBytes[4];
		uint32_t color32;
	};
	
	color32 = 0;
	
    while(!in.eof() && !in.fail()  && palette.colors.size() < 256){
		in.read(asBytes, 4);
		if(in.eof() || in.fail()){
			return;
		}
		Color c = Color::rgb888(color32);
		palette.colors.push_back(c);
	}
}





void PaletteImporter::importImagePal(const QString& filename){
    QImage img(filename);
    if(img.format() != QImage::Format_Indexed8){
        return;
    }
    palette.colors.clear();
    auto colors = img.colorTable();
    for(QColor color : colors){
        if(palette.colors.size() >= 256){return;}
        palette.colors.push_back(Color::qtColor(color));
    }
}



bool PaletteImporter::importImage(const QString& filename){
    QImage img(filename);
    if(img.height() * img.width() > 256){
        return false;
    }
    palette.colors.clear();
    for(int y = 0;y<img.height();y++){
        for(int x = 0;x<img.width();x++){
            QColor color = img.pixelColor(x, y);
            if(palette.colors.size() >= 256){return true;}
            palette.colors.push_back(Color::qtColor(color));
        }
    }
    return true;
}




void PaletteImporter::importGimp(std::istream &in){

    auto getNextNumber = [](std::string& line){
        int value = std::atoi(line.c_str());
        unsigned int i = 0;
        for(;i<line.size();i++){
            if(line[i] < '0' || line[i] > '9'){
                break;
            }
        }
        for(;i<line.size();i++){
            if(line[i] >= '0' && line[i] <= '9'){
                line = line.substr(i);
                break;
            }
        }
        return value;
    };

    std::string line;
	std::getline(in, line);	//gpl
    if(line.find("GIMP Palette") == std::string::npos){return;}
	std::getline(in, line);	//name
	std::getline(in, line);	//#


    palette.colors.clear();
    while(std::getline(in, line) && palette.colors.size() < 256){
        int r = getNextNumber(line);
        int g = getNextNumber(line);
        int b = getNextNumber(line);
		palette.colors.push_back(Color::rgb888(r,g,b));
	}
}



void PaletteImporter::importJASC(std::istream &in){

    std::string line;
	std::getline(in, line);	//JASC
    if(line.find("JASC-PAL") == std::string::npos){return;}
	std::getline(in, line);	//0100
    if(line.find("0100") == std::string::npos){return;}
	std::getline(in, line);	//color count
	
    palette.colors.clear();
    int colors = std::atoi(line.c_str());
    colors = (colors > 256) ? 256 : colors;
	
	for(int i = 0;i < colors; i++){
		std::getline(in, line);
		int r = std::atoi(line.substr(0,line.find(" ")).c_str());
		int g = std::atoi(line.substr(line.find(" ")+1,line.find_last_of(" ")).c_str());
		int b = std::atoi(line.substr(line.find_last_of(" ")+1).c_str());
		palette.colors.push_back(Color::rgb888(r,g,b));
	}

}







void PaletteImporter::importRIFF(std::istream& in){
    palette.colors.clear();
    char buf[16];
	in.read(buf, 16);

	if(buf[12] == 'd' && buf[13] == 'a' && buf[14] == 't' && buf[15] == 'a'){
		in.read(buf, 8); 	//6 useless bytes followed by 2 byte color count
		unsigned short colorcount = ((buf[7] << 8) | (buf[6]&0xFF));
        colorcount = (colorcount > 256) ? 256 : colorcount;

		for(int i = 0;i<colorcount;i++){
			in.read(buf, 4);
			palette.colors.push_back(Color::rgb888(buf[0], buf[1], buf[2]));
		}
	}
}




void PaletteImporter::importACT(std::istream& in){
    char buf[3*256 + 3];
    in.read(buf, 256*3 + 3);

    int colors = buf[769];
    if(!colors){colors = 256;}

    palette.colors.clear();
    for(int i = 0;i<colors;i++){
        palette.colors.push_back(Color::rgb888(buf[3*i], buf[3*i + 1], buf[3*i + 2]));
    }
}



void PaletteImporter::importPaintNet(std::istream& in){
    std::string line;
    palette.colors.clear();
    while(std::getline(in, line) && palette.colors.size() < 256){
        if(line[0] == ';'){continue;} // lines starting with ; are comments

        uint32_t a = strtoul(line.c_str(), NULL, 16);
        int r = (a & 0xFF0000) >> 16;
        int g = (a & 0xFF00) >> 8;
        int b = (a & 0xFF);
        palette.colors.push_back(Color::rgb888(r, g, b));
    }
}


void PaletteImporter::importNCLR(std::istream& in){
	int magic, size;
	in.read((char*)&magic, 4);
	if(magic != 0x4E434C52)return;
	in.seekg(0x20);
	in.read((char*)&size, 4);
	if(0x200-size > 0) 
		size = 0x200-size;
	in.seekg(0x28);
	importBinaryRGB555(in, size/2);
}


#include <QDebug>
bool PaletteImporter::importPal(const QString &file, PalFormat format, uint offset){
    qDebug() << "ImportPal("<<file<<","<<format << ","<<offset<< ")";
    if(!offset && format == PNG){
        return importImage(file);
    }else if(!offset && format == INDEXED){
        importImagePal(file);
        return true;
    }

    std::ifstream in(file.toStdString(), std::ios::binary);
    if(!in.is_open()){
        return false;
    }


    if(offset){
        in.seekg(offset);
        if(in.eof() || in.bad()){
            return false;
        }
    }


    switch(format){

		case RGB555:
			importBinaryRGB555(in);
            break;
			
		case RGB555_BIG:
			importBinaryRGB555_BigEndian(in);
            break;
		
		case RGB888:
			importBinaryRGB888(in);
            break;
		
		case RGBA32:
			importRGBA32(in);
            break;
		
		case JASC:
			importJASC(in);
            break;
		
		case GPL:
			importGimp(in);
            break;
		
		case RIFF:
			importRIFF(in);
            break;
		
        case ACT:
            importACT(in);
            break;

        case PAINTNET:
            importPaintNet(in);
        break;
	
	case NCLR:
	    importNCLR(in);
	break;

        default:
        //add some warning
        qDebug() << "importPal invalid type!\n";
        return false;
        break;

	}

    in.close();
    return true;
}
