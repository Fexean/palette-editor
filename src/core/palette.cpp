#include "core/palette.hpp"
#include <fstream>
#include <QImage>
#include <cstring>


void Palette::moveColor(int index, int newIndex){
	if(index == newIndex){
		return;
	}
	Color a = colors[index];
	colors.erase(colors.begin()+index);
	colors.insert(colors.begin()+newIndex, a);
	
}


void Palette::swapColors(int index, int index2){
	std::swap(colors[index], colors[index2]);
}


Color Palette::at(unsigned int index) const{
    if(index < colors.size()){
        return colors[index];
    }
    return Color::rgb888(0, 0, 0);
}



PalFormat Palette::getFileFormat(const QString& path){
    std::ifstream f(path.toStdString(), std::ios::binary);
    char buf[5] = {0};

    if(!f.is_open()){
        return INVALID;
    }

    f.read(buf, 4);
    f.close();

    if(!std::strcmp(buf, "GIMP")){
        return GPL;
    }

    if(!std::strcmp(buf, "JASC")){
        return JASC;
    }

    if(!std::strcmp(buf, "RIFF")){
        return RIFF;
    }

    if(buf[0] == '\x89' && buf[1] == 'P' && buf[2] == 'N' && buf[3] == 'G'){
        QImage img(path);
        if(img.format() == QImage::Format_Indexed8){
            return INDEXED;
        }
        return PNG;
    }

    if(!std::strcmp(buf, "RLCN")){
    	return NCLR;
    }

    if(path.endsWith(".gbapal") || path.endsWith(".gbcpal")){
        return RGB555;
    }

    if(path.endsWith(".act")){
        return ACT;
    }

    if(path.endsWith(".txt")){
        return PAINTNET;
    }

    return INVALID;
}
