#include "core/paletteExporter.hpp"
#include <QImage>
#include <fstream>


PaletteExporter::PaletteExporter(Palette &pal, LineFeedType newline) : palette(pal){
    linefeed = newline;
}


const std::string PaletteExporter::newline() const{
    switch(linefeed){
        case LineFeedType::CRLF:
        return "\xD\n";

        case LineFeedType::LF:
        return "\n";

        case LineFeedType::CR:
        return "\xD";
    };
    return "\n";
}



void PaletteExporter::exportBinaryRGB555(std::ostream &out){
	for(Color c : palette.colors){
		uint16_t bin = c.getBinaryRGB555();
		out.write((const char*)&bin, sizeof(uint16_t));
	}
}

void PaletteExporter::exportBinaryRGB555_BigEndian(std::ostream &out){
	for(Color c : palette.colors){
		uint16_t bin = c.getBinaryRGB555();
		bin = (bin >> 8) | (bin<<8);
		out.write((const char*)&bin, sizeof(uint16_t));
	}
}


void PaletteExporter::exportBinaryRGB888(std::ostream &out){
	for(Color c : palette.colors){
		uint32_t bin = c.getBinaryRGB888();
		out.write((const char*)&bin, sizeof(uint32_t)-1);
	}
}

void PaletteExporter::exportBinaryRGBA32(std::ostream &out){
	for(Color c : palette.colors){
		uint32_t bin = c.getBinaryRGB888() | 0xFF000000;
		out.write((const char*)&bin, sizeof(uint32_t));
	}
}



void PaletteExporter::exportGimpPal(std::ostream &out, const std::string &name){
    out << "GIMP Palette"<<newline()<<"Name: " << name << newline() << "#" << newline();
	for(Color c : palette.colors){
		uint32_t color = c.getBinaryRGB888();
		uint8_t r = color & 0xFF;
		uint8_t g = (color>>8) & 0xFF;
		uint8_t b = (color>>16) & 0xFF;
		out << (int)r << " "<<(int)g << " "<<(int)b;
        out << "\x09" << newline();
	}
}



void PaletteExporter::exportJASCPal(std::ostream &out){
    out << "JASC-PAL" << newline() << "0100"<< newline() <<palette.colors.size() << newline();
	for(Color c : palette.colors){
		uint32_t color = c.getBinaryRGB888();
		uint8_t r = color & 0xFF;
		uint8_t g = (color>>8) & 0xFF;
		uint8_t b = (color>>16) & 0xFF;
        out << (int)r << " "<<(int)g << " "<<(int)b << newline();
	}
}





void PaletteExporter::exportImagePal(const QString& filename){
    QImage img(filename);
    if(img.format() != QImage::Format_Indexed8){
        return;
    }
    QVector<QRgb> colortable;
    for(Color c: palette.colors){
        colortable.push_back(c.asQColor().rgb());
    }
    img.setColorTable(colortable);
    img.save(filename);
}
#include <QDebug>
void PaletteExporter::indexImageWithPal(const QString& filename){
    QImage img(filename);
    img = img.convertToFormat(QImage::Format_RGB32);
    QVector<QRgb> colortable;
    for(Color c: palette.colors){
        colortable.push_back(c.asQColor().rgb());
    }
    qDebug()<< "Saving as:"<<filename << img.convertToFormat(QImage::Format_Indexed8, colortable).save(filename);
}



void PaletteExporter::exportImage(const QString& filename){
    QImage img(palette.colors.size(), 1, QImage::Format_ARGB32);
    for(unsigned int x = 0; x < palette.colors.size(); x++){
        img.setPixelColor(x, 0, palette.colors[x].asQColor());
    }
    img.save(filename);
}





void PaletteExporter::exportRIFF(std::ostream &out){
	out.write("RIFF", 4);
    unsigned int fileLen = palette.colors.size()*4+12;	//length excluding riff header
	out.write((char*)&fileLen, 4);
	out.write("PAL\x20""data", 4);
	unsigned int chunksize = palette.colors.size()*4;
	out.write((char*)&chunksize, 4);	//CHUNK size
	out.write("\x00\x03", 2);	//PAL VERSION
	unsigned short colors = palette.colors.size();
	out.write((char*)&colors, 2);	//color count
	
	char buf[4] = {0};
	
	for(Color color : palette.colors){
		uint32_t c = color.getBinaryRGB888();
		uint8_t r = c & 0xFF;
		uint8_t g = (c>>8) & 0xFF;
		uint8_t b = (c>>16) & 0xFF;
		buf[0] = r;
		buf[1] = g;
		buf[2] = b;
		out.write(buf, 4);
	}
	
}




void PaletteExporter::exportACT(std::ostream& out){
    exportBinaryRGB888(out);
    for(int i = palette.colors.size();i<256;i++){
        const uint32_t bin = 0;
        out.write((const char*)&bin, 3);
    }
    out << '\0' <<(char)palette.colors.size() << "\xFF\xFF";
}



void PaletteExporter::exportPaintNet(std::ostream& out){
    out << std::hex;
    for(Color c : palette.colors){
        uint32_t raw = c.getBinaryRGB888BigEndian();
        raw |= 0xFF000000; //Alpha value
        out << raw << newline();
    }
}

void PaletteExporter::exportNCLR(std::ostream& out, bool is8bpp){
	//generic header
	const unsigned char header1[] = {0x52, 0x4C, 0x43, 0x4E, 0xFF, 0xFE, 0x00, 0x01};
	int size = palette.colors.size() * 2 + 0x28;
	int headerEnd = 0x010010;

	out.write((char*)header1, 8);
	out.write((char*)&size, 4);
	out.write((char*)&headerEnd, 4);

	//ttlp header
	out << "TTLP";
	size -= 0x10;
	out.write((char*)&size, 4);
	int bitdepth = is8bpp ? 4 : 3;
	out.write((char*)&bitdepth, 4);
	
	size = 0;
	out.write((char*)&size, 4);
	size = palette.colors.size() * 2;
	out.write((char*)&size, 4);
	size = 16;
	out.write((char*)&size, 4);
	exportBinaryRGB555(out);
}


#include <QDebug>
bool PaletteExporter::exportPal(const QString& file, PalFormat format, uint offset){
    qDebug() << "ExportPal("<<file<<","<<format << ","<<offset << ")";
    if(format == INVALID){
        return false;
    }else if(!offset && format == PNG){
        exportImage(file);
        return true;
    }else if(!offset && format == INDEXED){
        exportImagePal(file);
        return true;
    }

    std::fstream out;

    if(offset){
        out.open(file.toStdString(), std::ios::binary | std::fstream::in | std::fstream::out);
        if(!out.is_open()){ //file does not exist
            out.open(file.toStdString(), std::ios::binary | std::fstream::out);
            for(int i = 0;i<offset;i++){
                out << "\0";
            }
        }
        out.seekp(offset, std::ios_base::beg);
    }else{
        out.open(file.toStdString(), std::ios::binary | std::fstream::out);
        if(!out.is_open()){
            return false;
        }

    }

    switch(format){
		case RGB555:
			exportBinaryRGB555(out);
            break;
		
		case RGB555_BIG:
			exportBinaryRGB555_BigEndian(out);
            break;
		
		case RGB888:
			exportBinaryRGB888(out);
            break;
		
		case RGBA32:
			exportBinaryRGBA32(out);
            break;
		
		case JASC:
			exportJASCPal(out);
            break;
		
		case GPL:
			exportGimpPal(out, "");
            break;
		
		case RIFF:
			exportRIFF(out);
            break;

        case ACT:
            exportACT(out);
        break;

        case PAINTNET:
            exportPaintNet(out);
        break;
	
	case NCLR:
	    exportNCLR(out);
	break;

        default:
        qDebug() << "exportPal invalid type!\n";
        return false;
	}

    out.close();
    return true;
}
