#include "core/color.hpp"



uint32_t Color::getBinaryRGB888BigEndian(){
    uint8_t r8 = std::round(r * 255);
    uint8_t g8 = std::round(g * 255);
    uint8_t b8 = std::round(b * 255);
    return ((r8<<16) | (g8<<8)| b8);
}


Color Color::rgb888(unsigned char r, unsigned char g, unsigned char b){
	Color c;
	c.r = r/255.0;
	c.g = g/255.0;
	c.b = b/255.0;
	return c;
}

Color Color::rgb555(unsigned char r, unsigned char g, unsigned char b){
	Color c;
	c.r = r/31.0;
	c.g = g/31.0;
	c.b = b/31.0;
	return c;
}


Color Color::rgb555(unsigned short value){
	char r = value & 0x1F;
	char g = (value>>5) & 0x1F;
	char b = (value>>10) & 0x1F;
	return Color::rgb555(r,g,b);
}



Color Color::rgb888(uint32_t color){
	char r = color & 0xFF;
	char g = (color >> 8) & 0xFF;
	char b = (color >> 16) & 0xFF;
	return Color::rgb888(r, g, b);
}



Color hex(const std::string &hex){
	int value;
	if(hex[0] == '#'){
		value = std::strtoul(hex.substr(1).c_str(), NULL, 16);
	}else{
		value = std::strtoul(hex.c_str(), NULL, 16);
	}
	return Color::rgb888((value>>16) & 0xFF, (value >>8)&0xFF, value & 0xFF);
}


Color Color::qtColor(QColor color){
    return Color::rgb888(color.red(), color.green(), color.blue());
}


uint16_t Color::getBinaryRGB555(){
	uint8_t r5 = std::round(r * 31);
	uint8_t g5 = std::round(g * 31);
	uint8_t b5 = std::round(b * 31);
	return (r5 | (g5<<5)| (b5<<10));
}


uint32_t Color::getBinaryRGB888(){
	uint8_t r8 = std::round(r * 255);
	uint8_t g8 = std::round(g * 255);
	uint8_t b8 = std::round(b * 255);
	return (r8 | (g8<<8)| (b8<<16));
}

QColor Color::asQColor(){
    uint8_t r8 = std::round(r * 255);
    uint8_t g8 = std::round(g * 255);
    uint8_t b8 = std::round(b * 255);
    return QColor(r8, g8, b8);
}
