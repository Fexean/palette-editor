#include "UndoCommands.h"

ColorChangeCommand::ColorChangeCommand(PaletteModel* pal_, QModelIndex index, QColor newColor_, QUndoCommand* parent) :
    QUndoCommand(parent), model(pal_), palIndex(index), newColor(newColor_)
{}


void ColorChangeCommand::redo(){
    uint idx = (palIndex.column()+palIndex.row()*model->columnCount(QModelIndex()));
    oldColor = model->corepal.colors[idx].asQColor();
    model->corepal.colors[idx] = Color::qtColor(newColor);
    emit model->dataChanged(palIndex, palIndex);
}


void ColorChangeCommand::undo(){
    uint idx = (palIndex.column()+palIndex.row()*model->columnCount(QModelIndex()));
    model->corepal.colors[idx] = Color::qtColor(oldColor);
    emit model->dataChanged(palIndex, palIndex);
}







ColorInsertCommand::ColorInsertCommand(PaletteModel* pal_, QModelIndex src_, QModelIndex dst_, QUndoCommand* parent) :
    QUndoCommand(parent), model(pal_), src(src_), dst(dst_)
{}

void ColorInsertCommand::undo(){
    uint srcIdx = (src.column()+src.row()*model->columnCount(QModelIndex()));
    uint dstIdx = (dst.column()+dst.row()*model->columnCount(QModelIndex()));
    model->corepal.moveColor(dstIdx, srcIdx);
    emit model->dataChanged(src, dst);
}

void ColorInsertCommand::redo(){
    uint srcIdx = (src.column()+src.row()*model->columnCount(QModelIndex()));
    uint dstIdx = (dst.column()+dst.row()*model->columnCount(QModelIndex()));
    model->corepal.moveColor(srcIdx, dstIdx);
    emit model->dataChanged(src, dst);
}










PaletteClearCommand::PaletteClearCommand(PaletteModel* pal_, QUndoCommand* parent) :
    QUndoCommand(parent), model(pal_), oldPal(pal_->corepal)
{}

void PaletteClearCommand::undo(){
    model->corepal.colors = oldPal.colors;
    emit model->dataChanged(model->index(0,0), model->index(model->rowCount(), model->columnCount()));
}

void PaletteClearCommand::redo(){
    for(unsigned int i = 0 ; i< model->corepal.colors.size(); i++){
        model->corepal.colors[i] = Color::rgb888(127, 127, 127);
    }
    emit model->dataChanged(model->index(0,0), model->index(model->rowCount(), model->columnCount()));
}





PaletteResizeCommand::PaletteResizeCommand(PaletteModel* pal_, uint startIndex, uint colorcount, QUndoCommand* parent) :
    QUndoCommand(parent), model(pal_), oldPal(pal_->corepal)
{
    start = startIndex;
    count = colorcount;
}

void PaletteResizeCommand::undo(){
    model->corepal.colors = oldPal.colors;
    emit model->paletteResized();
    emit model->dataChanged(model->index(0,0), model->index(model->rowCount(), model->columnCount()));
}

void PaletteResizeCommand::redo(){
    if(start){
        model->corepal.colors.erase(model->corepal.colors.begin(), model->corepal.colors.begin() + start);
    }
    model->corepal.colors.resize(count, Color::rgb888(127, 127, 127));
    emit model->paletteResized();
    emit model->dataChanged(model->index(0,0), model->index(model->rowCount(), model->columnCount()));
}
