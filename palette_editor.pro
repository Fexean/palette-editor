#-------------------------------------------------
#
# Project created by QtCreator 2019-07-07T19:37:15
#
#-------------------------------------------------

QT       += core gui
QMAKE_CXXFLAGS += -std=gnu++11
INCLUDEPATH += "include"

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PaletteEditor
TEMPLATE = app


SOURCES += src/main.cpp\
    src/PaletteDelegate.cpp \
    src/UndoCommands.cpp \
    src/core/color.cpp \
    src/core/palette.cpp \
    src/core/paletteExporter.cpp \
    src/core/paletteImporter.cpp \
    src/mainwindow.cpp \
    src/PaletteModel.cpp \
    src/dialogs/PaletteFileDialog.cpp \
    src/dialogs/pngindexingdialog.cpp \
    src/dialogs/settingsdialog.cpp \
    src/dialogs/ColorDialog.cpp \
    src/dialogs/RangeDialog.cpp \

HEADERS  += include/mainwindow.h \
    include/PaletteDelegate.h \
    include/UndoCommands.h \
    include/core/color.hpp \
    include/core/palette.hpp \
    include/core/paletteExporter.hpp \
    include/core/paletteImporter.hpp \
    include/PaletteModel.h \
	include/dialogs/PaletteFileDialog.h \
    include/dialogs/SettingsDialog.h \
    include/dialogs/pngindexingdialog.h \
    include/dialogs/ColorDialog.h \
    include/dialogs/RangeDialog.h

FORMS    += ui/mainwindow.ui
