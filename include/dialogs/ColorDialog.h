#ifndef COLORDIALOG_H
#define COLORDIALOG_H

#include <QMouseEvent>
#include <QPainter>
#include <qdrawutil.h>
#include <QFrame>
#include <QDialog>
#include <QSpinBox>
#include <QSlider>
#include <QLabel>
#include <QHBoxLayout>
#include <QWindow>
#include <QTimer>



class QColorPicker : public QFrame
{
    Q_OBJECT
public:
    QColorPicker(QWidget* parent);
    ~QColorPicker();

    void setCrossVisible(bool visible);
public slots:
    void setCol(int v, int s);
    void setHue(int h);
    void setHue_noEmit(int h);

signals:
    void newCol(int h, int s, int v);

protected:
    QSize sizeHint() const override;
    void paintEvent(QPaintEvent*) override;
    void mouseMoveEvent(QMouseEvent *) override;
    void mousePressEvent(QMouseEvent *) override;
    void resizeEvent(QResizeEvent *) override;

private:
    int hue;
    int sat;
    int val;

    QPoint colPt();
    int huePt(const QPoint &pt);
    int satPt(const QPoint &pt);
    int valPt(const QPoint &pt);
    void setCol(const QPoint &pt);

    QPixmap pix;
    bool crossVisible;
};



class ColorHuePicker : public QWidget
{


    Q_OBJECT
public:
    ColorHuePicker(QWidget* parent=nullptr);
    ~ColorHuePicker();

public slots:
    void setHue_(int h);


signals:
    void newHue(int h);

protected:
    void paintEvent(QPaintEvent*) override;
    void mouseMoveEvent(QMouseEvent *) override;
    void mousePressEvent(QMouseEvent *) override;

private:
    enum { foff = 3, coff = 4 }; //frame and contents offset
    int hue;


    int y2hue(int y);
    int hue2y(int h);
    void setHue(int h);

    QPixmap *pix;
};



class ColorComponentSlider : public QWidget{
    Q_OBJECT

    int max;
    QHBoxLayout* internalLayout;
    QSpinBox* box;
    QSlider* slider;
    QLabel* text;

public:
    ColorComponentSlider(const QString& label, uint max, QWidget* parent = 0);
    void setValue(int n);
    int value() const;
    QSize sizeHint() const override;

signals:
    void valueChanged(int value);

};



class ColorDisplay : public QWidget{
    Q_OBJECT

public:
    ColorDisplay(QWidget* parent = 0);
    void setColor(const QColor& c);
    QColor color();

private:
    QColor m_color;

    void paintEvent(QPaintEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *) override;

public slots:
    void setCol(int h, int s, int v);

signals:
    void clicked();
};


class QColorPickingEventFilter;

class ColorDialog : public QDialog{

    Q_OBJECT

public:
    ColorDialog(const QColor& initial, QWidget *parent = nullptr);
    ColorDialog(QWidget *parent = nullptr);
    ~ColorDialog();



    QColor color();
    void setColor(const QColor& color);
    void setPrevColor(const QColor& color);

    bool handleColorPickingMouseMove(QMouseEvent*);
    bool handleColorPickingMouseButtonRelease(QMouseEvent*);
    bool handleColorPickingKeyPress(QKeyEvent*);

private:
    QColorPickingEventFilter *pickScreenColorEventFilter;
#ifdef Q_OS_WIN32
    QTimer *updateTimer;
    QWindow dummyTransparentWindow;
#endif

    ColorHuePicker *colorSlider;
    QColorPicker *colorMap;
    QColor m_color;

    ColorComponentSlider *hSlider, *sSlider, *vSlider, *rSlider, *gSlider, *bSlider;

    ColorDisplay *prevColor;
    ColorDisplay *currentColor;

    void init();
    void updateColorFromSlidersHSV();
    void updateColorFromSlidersRGB();
    QColor grabScreenColor(const QPoint &p);
    void stopPickScreenColor();

private slots:
    void newHsv(int h, int s, int v);
    void pickScreenColor();
    void updatePickScreenColorWindows();

};



#endif // COLORDIALOG_H
