#ifndef PALETTEFILEDIALOG_H
#define PALETTEFILEDIALOG_H

#include <QDialog>
#include <QPushButton>
#include <QComboBox>
#include <QLineEdit>
#include <QLabel>
#include <QVBoxLayout>
#include <QDialogButtonBox>
#include <QDir>

#include <QFileDialog>


class PaletteFileDialog : public QDialog{

    Q_OBJECT


    enum class Mode {EXPORT, IMPORT} mode;

    QPushButton* browseButton;
    QLabel* formatLabel, *fileLabel;
    QComboBox* formatBox;
    QLineEdit* filepathLine;
    QLineEdit* offsetLine;
    QDialogButtonBox* buttons;
    QWidget *extension;
    QVBoxLayout* mainlayout;



public:
    QString filepath, currentDir;
    int palformat;
    uint offset;

    PaletteFileDialog(QWidget* parent = 0, const QString& file = "", int format = 0, uint fileoffset = 0);
    static PaletteFileDialog* exportDialog(QWidget* parent = 0, const QString& file = "", int format = 0, uint fileoffset = 0);
    static PaletteFileDialog* importDialog(QWidget* parent = 0, const QString& file = "", int format = 0, uint fileoffset = 0);

private slots:
    void browseFiles();

};

#endif // PALETTEFILEDIALOG_H
