#ifndef PNGINDEXINGDIALOG_H
#define PNGINDEXINGDIALOG_H

#include <QObject>
#include <QDialog>

#include <QLineEdit>
#include <QRadioButton>

class PngIndexingDialog : public QDialog
{
     Q_OBJECT
public:
    PngIndexingDialog(QWidget* parent = 0);

    QLineEdit* filepathLine;
    QRadioButton* radioIndex;
    QRadioButton* radioReplacePal;

private slots:
    void browseFiles();
};






#endif // PNGINDEXINGDIALOG_H
