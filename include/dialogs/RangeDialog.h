#ifndef RANGEDIALOG_H
#define RANGEDIALOG_H

#include <QDialog>
#include <QSpinBox>
#include "core/paletteExporter.hpp"

class RangeDialog : public QDialog{
	Q_OBJECT
	
	QSpinBox* startLine;
	QSpinBox* countLine;
	
	public:
	RangeDialog(QWidget* parent, int colorcount);
	
    int start, count;
};

#endif
