#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include <QComboBox>
#include <QDialogButtonBox>
#include "core/paletteExporter.hpp"



class SettingsDialog : public QDialog
{
    Q_OBJECT

    QComboBox* newlineBox;

    QComboBox* editorBox;
    QDialogButtonBox* buttons;

public:
    SettingsDialog(QWidget* parent = 0, LineFeedType defaultNewline = LineFeedType::CRLF, int defaultEditor = 0);

    LineFeedType newline;
    int editorType;
};

#endif // SETTINGSDIALOG_H
