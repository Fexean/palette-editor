#pragma once

#include "color.hpp"
#include <vector>
#include <algorithm>


enum PalFormat {
	JASC,
	GPL,
	RIFF,
       	ACT,
       	RGB555,
       	RGB555_BIG,
       	RGB888,
       	RGBA32,
       	INDEXED,
       	PNG,
       	PAINTNET,
	NCLR,
	INVALID,
};


class Palette{
	public:
	
    std::vector<Color> colors;
	
    Color at(unsigned int index) const;
	void moveColor(int index, int newIndex);
	void swapColors(int index, int index2);
	
    static PalFormat getFileFormat(const QString& path);
};
