#pragma once

#include "palette.hpp"
#include <iostream>
#include <cmath>

enum class LineFeedType {CRLF, LF, CR};

class PaletteExporter{
	Palette &palette;


    const std::string newline() const;

public:

    LineFeedType linefeed;

    PaletteExporter(Palette &pal, LineFeedType newline = LineFeedType::CRLF);
    bool exportPal(const QString& file, PalFormat format, uint offset = 0);
	
	void exportBinaryRGB555(std::ostream &out);
	void exportBinaryRGB555_BigEndian(std::ostream &out);
	void exportBinaryRGB888(std::ostream &out);
	void exportBinaryRGBA32(std::ostream &out);
	void exportGimpPal(std::ostream &out, const std::string &name);
	void exportJASCPal(std::ostream &out);
	void exportRIFF(std::ostream& out);
    void exportACT(std::ostream& out);
    void exportPaintNet(std::ostream& out);
    void exportNCLR(std::ostream& out, bool is8bpp = false);

    void exportImage(const QString& filename);
    void exportImagePal(const QString& filename);
    void indexImageWithPal(const QString& filename);
};
