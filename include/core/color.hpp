#pragma once

#include <string>
#include <cstdlib>
#include <cmath>
#include <QColor>

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;

class Color{
	
	public:
	
	double r,g,b;

	uint32_t getBinaryRGB888();
    uint32_t getBinaryRGB888BigEndian();
	uint16_t getBinaryRGB555();
    QColor asQColor();
	
	static Color rgb888(unsigned char r, unsigned char g, unsigned char b);
	static Color rgb888(uint32_t color);
	static Color rgb555(unsigned short value);
	static Color rgb555(unsigned char r, unsigned char g, unsigned char b);
    static Color qtColor(QColor color);
};



	Color rgb555(unsigned short value);
	Color rgb888(uint32_t color);
	
	Color rgb555(unsigned char r, unsigned char g, unsigned char b);
	Color rgb888(unsigned char r, unsigned char g, unsigned char b);
	Color hex(const std::string &hex);
	
	
	
	
