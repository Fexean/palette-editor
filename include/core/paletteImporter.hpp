#pragma once

#include "palette.hpp"
#include <iostream>
#include <fstream>

class PaletteImporter{
	
	Palette &palette;
	
public:
	PaletteImporter(Palette &pal);
	
    bool importPal(const QString &file, PalFormat format, uint offset = 0);
	
	void importBinaryRGB555_BigEndian(std::istream &in);
	void importBinaryRGB555(std::istream &in, int maxcolors = 256);
	void importBinaryRGB888(std::istream &in);
	void importRGBA32(std::istream& in);
	void importGimp(std::istream &in);
	void importJASC(std::istream &in);
	void importRIFF(std::istream& in);
    void importACT(std::istream& in);
    void importPaintNet(std::istream& in);
    void importNCLR(std::istream& in);

    bool importImage(const QString& filename);
    void importImagePal(const QString& filename);
};
