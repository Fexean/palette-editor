#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include "PaletteModel.h"
#include "PaletteDelegate.h"

#include "core/palette.hpp"
#include "core/paletteImporter.hpp"
#include "core/paletteExporter.hpp"


namespace Ui {
class MainWindow;
}


class MainWindow : public QMainWindow
{
    Q_OBJECT

    constexpr static int MaxRecentFiles = 10;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void importPalette(const QString& path);

private:
    Ui::MainWindow *ui;

    QUndoStack* undoStack;
    PaletteModel *PalModel;
    QString lastFile;
    int lastFormat;
    uint lastOffset;
    LineFeedType newline;
    EditorType colordiag;

    QSettings ini;

    PaletteDelegate *palDelegate;
    QAction *recentFileActs[MaxRecentFiles];

    void saveSettings();
    bool importPalette(const QString& path, int format, uint offset = 0);
    bool exportPalette(const QString& path, int format, uint offset = 0);

    void dropEvent(QDropEvent* event) override;
    void dragEnterEvent(QDragEnterEvent *event) override;
    void addToRecentFiles(const QString &filepath, int format, int offset);
    void updateRecentFiles();

public slots:
    void openColorCountMenu();
    void openRangeDialog();
    void openPaletteImporter();
    void openPaletteExporter();
    void openImageIndexer();
    void openSettingsMenu();
    void clearPalette();
    void onSave();
    void loadRecentFile();
    void UpdateGridSize();
};

#endif // MAINWINDOW_H
