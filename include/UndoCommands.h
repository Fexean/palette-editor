#ifndef COLORCHANGECOMMAND_H
#define COLORCHANGECOMMAND_H

#include <QUndoCommand>
#include "core/palette.hpp"
#include "PaletteModel.h"

class ColorChangeCommand : public QUndoCommand
{

    PaletteModel *model;
    QModelIndex palIndex;
    QColor oldColor, newColor;

public:
    void redo() override;
    void undo() override;

    ColorChangeCommand(PaletteModel* pal_, QModelIndex index, QColor newColor_, QUndoCommand* parent = 0);
};




class ColorInsertCommand : public QUndoCommand{
    PaletteModel *model;
    QModelIndex src, dst;
public:
    void redo() override;
    void undo() override;

    ColorInsertCommand(PaletteModel* pal_, QModelIndex src_, QModelIndex dst_, QUndoCommand* parent = 0);
};



class PaletteClearCommand : public QUndoCommand{
    PaletteModel *model;
    Palette oldPal;
public:
    void redo() override;
    void undo() override;

    PaletteClearCommand(PaletteModel* pal_, QUndoCommand* parent = 0);
};



class PaletteResizeCommand : public QUndoCommand{
    PaletteModel *model;
    Palette oldPal;
    uint start, count;

public:
    void redo() override;
    void undo() override;

    PaletteResizeCommand(PaletteModel* pal_, uint startIndex, uint colorcount, QUndoCommand* parent = 0);
};

#endif // COLORCHANGECOMMAND_H
