#ifndef PALETTEDELEGATE_H
#define PALETTEDELEGATE_H

#include <QStyledItemDelegate>
#include <QPainter>

enum class EditorType{CUSTOM_COLORDIALOG, QCOLORDIALOG};

class PaletteDelegate : public QStyledItemDelegate{
    Q_OBJECT


     EditorType editorType;

public:

    PaletteDelegate(QObject *parent);
    void setEditor(EditorType editor);


    QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& opt, const QModelIndex& i)const override;
    void setEditorData(QWidget* editor, const QModelIndex& i) const override;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &/* index */) const override;
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
};


#endif // PALETTEDELEGATE_H
