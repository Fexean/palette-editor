#ifndef PALETTEGRID_H
#define PALETTEGRID_H

#include <QtWidgets/QTableView>
#include <QMouseEvent>
#include <QUndoStack>

#include <QColor>
#include <QHeaderView>
#include <QBrush>

#include <QMimeData>

#include "core/palette.hpp"




enum class PalDragMode {SWAP, INSERT, CLONE, BLEND};

class PaletteModel : public QAbstractTableModel{

    Q_OBJECT
    QUndoStack* undostack;


public:
    Palette corepal;
    PalDragMode dragmode;
    float blendRate;

    PaletteModel(QObject *parent, QUndoStack* undoStack);
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex & parent = QModelIndex()) const override;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;

    //Drag & Drop functions
    Qt::DropActions supportedDropActions() const override;
    QStringList mimeTypes() const override;
    QMimeData* mimeData(const QModelIndexList & indexes) const override;
    bool dropMimeData(const QMimeData * dat, Qt::DropAction action, int row, int column, const QModelIndex & parent) override;


    void resizePal(int colorcount);
    void limitPal(int start, int count);
    uint getColorCount() const;
    void update();
    bool indexWithingPal(const QModelIndex& idx) const;
    void blendColors(const QModelIndex& dstIdx, const QColor& color);
	

signals:
    void paletteResized();

};




#endif // PALETTEGRID_H

